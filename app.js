let path = require('path');
const packageJSON = require('./package.json');
const commander = require('commander');
const appConfig = require('./config');
commander
  .version(packageJSON.version)
  .option('-p, --path <name>', '处理目录名')
  .option('-v, --version', '版本信息')
  .parse(process.argv);
if (commander.path) {
  let projectPath = path.join(appConfig.projectRoot, commander.path, './');
  let config;
  try {
    config = require('./' + projectPath + 'config.js');
    if (config) {
      let handler = require(appConfig.functionPath + config.handler);
      handler(config, projectPath);
    }
  } catch (err) {
    throw err;
  }
} else {
  throw new Error('请输入path参数： npm run app -- --path')
}
