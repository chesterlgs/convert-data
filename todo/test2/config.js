module.exports = {
  handler: 'many2Many',
  source: {
    path: './source/',
    reg: /[\"\']*name[\"\']*:[\s\r\t]*[\"\']*([\u4e00-\u9fa5]+)[\"\']*\,[\s\r\t\n]*[\"\']*sex[\"\']*:[\s\r\t]*[\"\']*([\u4e00-\u9fa5]+)[\"\']*/g,
    keys: ['name', 'sex'],
    suffix: ['.txt', '.json'] // 指定后缀
  },
  targets: {
    path: './dists',
    nameFormat: 'camelCase',
    suffix: '.json',
    render: `export default [
        <%_.each(items,function(item,i){%>
            {
              name: '<%=item.name%>',
              sex: '<%=item.sex%>'
            }
            <%if(i !== items.length - 1) {%>
              ,
              <%}%>
          <%})%>
    ]`
  }
};
