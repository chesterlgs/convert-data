// 一个源文件转多个目标文件
module.exports = {
  handler: 'one2Many',
  source: {
    path: './source.txt',
    // startReg: '',
    // endReg: '',
    reg: /([A-Z]+)[\s\r\t]*([\u4e00-\u9fa5]+)/g,
    keys: ['code', 'name']
  },
  targets: {
    path: './dists',
    files: [
      {
        fileName: 'countryCode.js',
        render: `
            export default {
              <%_.each(items,function(item,i){%>
                '<%= item.name%>': '<%=item.code%>'
                <%if(i !== items.length - 1) {%>
                  ,
                  <%}%>
              <%})%>
            }`
      },
      {
        fileName: 'countryName.js',
        render: `
            export default {
              <%_.each(items,function(item,i){%>
                '<%= item.code%>':'<%=item.name%>'
                <%if(i !== items.length - 1) {%>
                  ,
                  <%}%>
              <%})%>
            }`
      },
      {
        fileName: 'countryArr.js',
        render: `
            export default [
              <%_.each(items,function(item,i){%>
                  <%= JSON.stringify(item)%>
                  <%if(i !== items.length - 1) {%>
                    ,
                    <%}%>
                <%})%>
            ]
            `
      }
    ]
  }
};
