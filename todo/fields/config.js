// 一个源文件转多个目标文件
module.exports = {
  handler: 'one2Many',
  source: {
    path: './source.txt',
    // startReg: '',
    // endReg: '',
    reg: /([A-Z0-9]{3,})[\s\r\t]+/g,
    keys: ['code']
  },
  targets: {
    path: './dists',
    files: [
      {
        fileName: 'fields.js',
        render: `
              export default [
                <%_.each(items,function(item,i){%>
                  '<%=item.code%>'
                  <%if(i !== items.length - 1) {%>
                    ,
                    <%}%>
                <%})%>
                  ]`
      }
    ]
  }
};
