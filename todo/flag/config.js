module.exports = {
  handler: 'one2Many',
  source: {
    path: './source.txt',
    reg: /([\u4e00-\u9fa5]+)[\r\t\s ]*（([a-z]+)[\r\t\s ]*）/g,
    keys: ['name', 'code']
  },
  targets: {
    path: './dists',
    files: [
      {
        fileName: 'flagObj.js',
        render: `
        export default {
          <%_.each(items,function(item,i){%>
            '<%= item.name%>': '<%=item.code%>'
            <%if(i !== items.length - 1) {%>
              ,
              <%}%>
          <%})%>
        }`
      },
      {
        fileName: 'flagArr.js',
        render: `
          export default [
            <%_.each(items,function(item,i){%>
                <%= JSON.stringify(item)%>
                <%if(i !== items.length - 1) {%>
                  ,
                  <%}%>
              <%})%>
          ]
          `
      }
    ]
  }
};
