module.exports = {
  handler: 'many2One',
  source: {
    path: './source/',
    reg: /[\"\']*name[\"\']*:[\s\r\t]*[\"\']*([\u4e00-\u9fa5]+)[\"\']*\,[\s\r\t\n]*[\"\']*sex[\"\']*:[\s\r\t]*[\"\']*([\u4e00-\u9fa5]+)[\"\']*/g,
    keys: ['name', 'sex'],
    suffix: ['.txt'] // 指定后缀
  },
  targets: {
    path: './dists',
    fileName: 'test.js',
    render: `export default [
        <%_.each(items,function(item,i){%>
            {
              name: '<%=item.name%>',
              sex: '<%=item.sex%>'
            }
            <%if(i !== items.length - 1) {%>
              ,
              <%}%>
          <%})%>
    ]`
  }
};
