export default [{
  'code': '350205291200',
  'name': '厦门市公安局后井边防派出所',
  'lng': '118.003979',
  'lat': '24.461881'
}, {
  'code': '350298700000',
  'name': '厦门市公安局水上派出所',
  'lng': '118.081822',
  'lat': '24.473467'
}, {
  'code': '350298710000',
  'name': '厦门市公安局公交派出所',
  'lng': '118.081713',
  'lat': '24.473205'
}, {
  'code': '350298720000',
  'name': '厦门市公安局象屿保税区派出所',
  'lng': '118.093323',
  'lat': '24.511891'
}, {
  'code': '350298730000',
  'name': '厦门市公安局集美公交派出所',
  'lng': '118.098884',
  'lat': '24.624357'
}, {
  'code': '350298900000',
  'name': '厦门市公安局沙坡尾边防派出所',
  'lng': '118.095154',
  'lat': '24.447073'
}, {
  'code': '350205697000',
  'name': '厦门市公安局海沧森林派出所',
  'lng': '118.062948',
  'lat': '24.51436'
}, {
  'code': '350211697000',
  'name': '厦门市公安局集美森林派出所',
  'lng': '118.047144',
  'lat': '24.635624'
}, {
  'code': '350212697000',
  'name': '厦门市公安局同安森林派出所',
  'lng': '118.124343',
  'lat': '24.707275'
}, {
  'code': '350213697000',
  'name': '厦门市公安局翔安森林派出所',
  'lng': '118.262904',
  'lat': '24.619777'
}, {
  'code': '350206700000',
  'name': '厦门市公安局湖里派出所',
  'lng': '118.115448',
  'lat': '24.515012'
}, {
  'code': '350206710000',
  'name': '厦门市公安局禾山派出所',
  'lng': '118.15834',
  'lat': '24.524405'
}, {
  'code': '350206720000',
  'name': '厦门市公安局殿前派出所',
  'lng': '118.127873',
  'lat': '24.525023'
}, {
  'code': '350206730000',
  'name': '厦门市公安局江头派出所',
  'lng': '118.147065',
  'lat': '24.502659'
}, {
  'code': '350206740000',
  'name': '厦门市公安局金山派出所',
  'lng': '118.159251',
  'lat': '24.50927'
}, {
  'code': '350211700000',
  'name': '厦门市公安局集美派出所',
  'lng': '118.102518',
  'lat': '24.578732'
}, {
  'code': '350211710000',
  'name': '厦门市公安局后溪派出所',
  'lng': '118.054453',
  'lat': '24.632511'
}, {
  'code': '350211730000',
  'name': '厦门市公安局侨英派出所',
  'lng': '118.11143',
  'lat': '24.601141'
}, {
  'code': '350211750000',
  'name': '厦门市公安局杏林派出所',
  'lng': '118.04712',
  'lat': '24.566541'
}, {
  'code': '350212700000',
  'name': '厦门市公安局洪塘派出所',
  'lng': '118.203161',
  'lat': '24.720498'
}, {
  'code': '350212710000',
  'name': '厦门市公安局五显派出所',
  'lng': '118.179348',
  'lat': '24.752707'
}, {
  'code': '350212720000',
  'name': '厦门市公安局大同派出所',
  'lng': '118.159755',
  'lat': '24.737716'
}, {
  'code': '350212730000',
  'name': '厦门市公安局新民派出所',
  'lng': '118.138478',
  'lat': '24.693903'
}, {
  'code': '350212740000',
  'name': '厦门市公安局祥平派出所',
  'lng': '118.154343',
  'lat': '24.718858'
}, {
  'code': '350212760000',
  'name': '厦门市公安局汀溪派出所',
  'lng': '118.143162',
  'lat': '24.786591'
}, {
  'code': '350212770000',
  'name': '厦门市公安局莲花派出所',
  'lng': '118.082973',
  'lat': '24.744262'
}, {
  'code': '350211760000',
  'name': '厦门市公安局集美学村派出所',
  'lng': '118.102518',
  'lat': '24.578732'
}, {
  'code': '350203820000',
  'name': '厦门市公安局大学路派出所',
  'lng': '118.105991',
  'lat': '24.446933'
}, {
  'code': '350203700000',
  'name': '厦门市公安局鼓浪屿派出所',
  'lng': '118.071884',
  'lat': '24.451782'
}, {
  'code': '350203710000',
  'name': '厦门市公安局中华派出所',
  'lng': '118.089587',
  'lat': '24.463995'
}, {
  'code': '350203720000',
  'name': '厦门市公安局碧山派出所',
  'lng': '118.097302',
  'lat': '24.445625'
}, {
  'code': '350203730000',
  'name': '厦门市公安局滨海派出所',
  'lng': '118.147576',
  'lat': '24.440581'
}, {
  'code': '350203740000',
  'name': '厦门市公安局鹭江派出所',
  'lng': '118.090771',
  'lat': '24.464944'
}, {
  'code': '350203770000',
  'name': '厦门市公安局筼筜派出所',
  'lng': '118.110406',
  'lat': '24.495029'
}, {
  'code': '350203780000',
  'name': '厦门市公安局滨北派出所',
  'lng': '118.093252',
  'lat': '24.483675'
}, {
  'code': '350203790000',
  'name': '厦门市公安局莲前派出所',
  'lng': '118.146476',
  'lat': '24.481061'
}, {
  'code': '350203800000',
  'name': '厦门市公安局嘉莲派出所',
  'lng': '118.125617',
  'lat': '24.488699'
}, {
  'code': '350203810000',
  'name': '厦门市公安局前埔派出所',
  'lng': '118.181584',
  'lat': '24.476207'
}, {
  'code': '350205700000',
  'name': '厦门市公安局海沧派出所',
  'lng': '117.987852',
  'lat': '24.467173'
}, {
  'code': '350205720000',
  'name': '厦门市公安局新阳派出所',
  'lng': '118.016663',
  'lat': '24.533271'
}, {
  'code': '350205730000',
  'name': '厦门市公安局东孚派出所',
  'lng': '117.938712',
  'lat': '24.563271'
}, {
  'code': '350213700000',
  'name': '厦门市公安局大嶝派出所',
  'lng': '118.328415',
  'lat': '24.559171'
}, {
  'code': '350213710000',
  'name': '厦门市公安局新店派出所',
  'lng': '118.249186',
  'lat': '24.609262'
}, {
  'code': '350213720000',
  'name': '厦门市公安局马巷派出所',
  'lng': '118.251419',
  'lat': '24.675333'
}, {
  'code': '350213730000',
  'name': '厦门市公安局内厝派出所',
  'lng': '118.284319',
  'lat': '24.670697'
}, {
  'code': '350299700000',
  'name': '厦门市公安局机场派出所',
  'lng': '118.128574',
  'lat': '24.535646'
}, {
  'code': '350203760000',
  'name': '厦门市公安局梧村派出所',
  'lng': '118.132474',
  'lat': '24.476732'
}, {
  'code': '350211720000',
  'name': '厦门市公安局灌口派出所',
  'lng': '118.006205',
  'lat': '24.607372'
}, {
  'code': '350213740000',
  'name': '厦门市公安局新圩派出所',
  'lng': '118.258754',
  'lat': '24.740168'
}, {
  'code': '350299710000',
  'name': '厦门市公安局候机楼派出所',
  'lng': '118.143539',
  'lat': '24.540163'
}, {
  'code': '350203750000',
  'name': '厦门市公安局开元派出所',
  'lng': '118.095474',
  'lat': '24.467081'
}, {
  'code': '350205710000',
  'name': '厦门市公安局钟山派出所',
  'lng': '118.043133',
  'lat': '24.499809'
}, {
  'code': '350211740000',
  'name': '厦门市公安局杏滨派出所',
  'lng': '118.03936',
  'lat': '24.590463'
}, {
  'code': '350212750000',
  'name': '厦门市公安局西柯派出所',
  'lng': '118.145679',
  'lat': '24.670705'
}, {
  'code': '350200430500',
  'name': '厦门市公安局户政处派出所工作指导科',
  'lng': '118.166216',
  'lat': '24.520319'
}, {
  'code': '350203291100',
  'name': '厦门市公安局何厝边防派出所',
  'lng': '118.199669',
  'lat': '24.494119'
}, {
  'code': '350203291200',
  'name': '厦门市公安局曾厝垵边防派出所',
  'lng': '118.123197',
  'lat': '24.437372'
}, {
  'code': '350205291100',
  'name': '厦门市公安局嵩屿边防派出所',
  'lng': '118.046572',
  'lat': '24.457035'
}, {
  'code': '350211291200',
  'name': '厦门市公安局凤安边防派出所',
  'lng': '118.127535',
  'lat': '24.600507'
}, {
  'code': '350206291100',
  'name': '厦门市公安局五通边防派出所',
  'lng': '118.202019',
  'lat': '24.509127'
}, {
  'code': '350206291200',
  'name': '厦门市公安局高崎边防派出所',
  'lng': '118.115341',
  'lat': '24.555384'
}, {
  'code': '350212291100',
  'name': '厦门市公安局潘涂边防派出所',
  'lng': '118.150754',
  'lat': '24.650072'
}, {
  'code': '350213291100',
  'name': '厦门市公安局琼头边防派出所',
  'lng': '118.202567',
  'lat': '24.634552'
}, {
  'code': '350213291200',
  'name': '厦门市公安局莲河边防派出所',
  'lng': '118.341272',
  'lat': '24.600511'
}, {
  'code': '350213291300',
  'name': '厦门市公安局大嶝边防派出所',
  'lng': '118.328415',
  'lat': '24.559171'
}, {
  'code': '350213291400',
  'name': '厦门市公安局沃头边防派出所',
  'lng': '118.207657',
  'lat': '24.57719'
}, {
  'code': '350211291300',
  'name': '厦门市公安局马銮湾边防派出所',
  'lng': '118.030374',
  'lat': '24.561467'
}]
