module.exports = {
    handler: 'one2Many',
    source: {
      path: './source.txt',
      reg:  /(\d{12})[\s\r\t]*([\u4e00-\u9fa5]+)[\s\r\t]*(\d{3}\.\d{4,6})[\s\r\t]*(\d{2}\.\d{4,6})/g,
      keys: ['code', 'name', 'lng', 'lat']
    },
    targets: {
      path: './dists',
      files: [
        {
          fileName: 'pcsCodeObj.js',
          render: `
          export default {
            <%_.each(items,function(item,i){%>
              '<%= item.code%>': {
                name: '<%=item.name%>',
                lng: <%=item.lng%>,
                lat: <%=item.lat%>
              }
              <%if(i !== items.length - 1) {%>
                ,
                <%}%>
            <%})%>
          }`
        },
        {
          fileName: 'pcsNameObj.js',
          render: `
          export default {
            <%_.each(items,function(item,i){%>
              '<%= item.name%>': {
                name: '<%=item.code%>',
                lng: <%=item.lng%>,
                lat: <%=item.lat%>
              }
              <%if(i !== items.length - 1) {%>
                ,
                <%}%>
            <%})%>
          }`
        },
        {
          fileName: 'pcsArr.js',
          render: `
            export default [
              <%_.each(items,function(item,i){%>
                  <%= JSON.stringify(item)%>
                  <%if(i !== items.length - 1) {%>
                    ,
                    <%}%>
                <%})%>
            ]
            `
        }
      ]
    }
  };
  