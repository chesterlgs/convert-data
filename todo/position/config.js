module.exports = {
  handler: 'one2Many',
  source: {
    path: './source.txt',
    reg: /\'([\u4e00-\u9fa5]+)\'\:[\s\r\t]*\[([\-]*\d+\.\d+),[\s\r\t]*([\-]*\d+\.\d+)\]/g,
    keys: ['srcname', 'lng', 'lat']
  },
  targets: {
    path: './dists',
    files: [
      {
        fileName: 'countryLoc.js',
        render: `
            export default [
              <%_.each(items,function(item,i){%>
                  <%= JSON.stringify(item)%>
                  <%if(i !== items.length - 1) {%>
                    ,
                    <%}%>
                <%})%>
            ]
            `
      },
      {
        fileName: 'countryLoc2.js',
        render: `
        export default [
          <%_.each(items,function(item,i){%>
            {
              srcname: '<%=item.srcname%>',
              lng: <%=item.lng%>,
              lat: <%=item.lat%>
            }
            <%if(i !== items.length - 1) {%>
              ,
              <%}%>
          <%})%>
            ]`
      }

    ]
  }
};
