# convert-data

#### 文件提取及格式化工具(convert-data)
用于文件正则提取并通过自定义的模板生成文件，也可用于文件合并

#### 安装

```
git clone https://gitee.com/chesterlgs/convert-data.git
npm install
```
#### 配置文件目录和方法目录
./config.js文件
```
module.exports = {
    projectRoot: './todo/', // 待处理文件的根目录
    functionPath: './lib/fun/' // 处理方法
}
```
#### 配置文件编写
举个例子
```
module.exports = {
  handler: 'many2One',
  source: {
    path: './source/',
    reg: /[\"\']*name[\"\']*:[\s\r\t]*[\"\']*([\u4e00-\u9fa5]+)[\"\']*\,[\s\r\t\n]*[\"\']*sex[\"\']*:[\s\r\t]*[\"\']*([\u4e00-\u9fa5]+)[\"\']*/g,
    keys: ['name', 'sex'],
    suffix: ['.txt'] // 指定后缀
  },
  targets: {
    path: './dists',
    fileName: 'test.js',
    render: `export default [
        <%_.each(items,function(item,i){%>
            {
              name: '<%=item.name%>',
              sex: '<%=item.sex%>'
            }
            <%if(i !== items.length - 1) {%>
              ,
              <%}%>
          <%})%>
    ]`
  }
};
```


#### 命令说明

```
npm run app -- --path [name]
```
运行后，会在./todo/文件夹下查找文件夹名称为name下面的config.js,按照config.js的配置进行文件转换。

#### config.js配置参数
项目给了三个方法:

many2Many: 从多个文件按照规则转换至多个文件

many2One: 从多个文件按照规则合并至一个文件

one2Many: 从一个文件按照规则生成多个文件

用法参考todo文件夹中的示例

下面是配置项的含义：

many2Many: 

> handler:  处理方法的名称

> source.path   源文件夹

> source.reg   处理规则的正则表达式，如果省略，直接取文件字符串内容，而非对象数组

> source.keys   转换的字段名，将与正则表达式匹配的子串一一对应，并将匹配后的内容转换成一个对象数组

> source.suffix   指定的后缀名，可通过后缀名指定要处理的文件类型，省略则处理全部

> targets.path 处理后输出的目录名

> targets.nameFormat 使用lodash的哪个方法处理文件名

> targets.suffix 转换后文件扩展名，可省略，默认.js

> targets.render 转换的模板，使用lodash的template语法，如果省略，直接输出不经转换的字符串

many2One: 

> handler:  处理方法的名称

> source.path   源文件夹

> source.reg   处理规则的正则表达式，如果省略，直接取文件字符串内容，而非对象数组

> source.keys   转换的字段名，将与正则表达式匹配的子串一一对应，并将匹配后的内容转换成一个对象数组

> source.suffix   指定的后缀名，可通过后缀名指定要处理的文件类型，省略则处理全部

> targets.path 处理后输出的目录名

> targets.fileName 目标文件名

> targets.render 转换的模板，使用lodash的template语法，如果省略，直接输出不经转换的字符串

同时省略source.reg和targets.render, 可以实现合并文件的效果

one2Many: 

> handler:  处理方法的名称

> source.path   源文件夹

> source.reg   处理规则的正则表达式

> source.keys   转换的字段名，将与正则表达式匹配的子串一一对应，并将匹配后的内容转换成一个对象数组

> targets.path 处理后输出的目录名

> targets.files[x].fileName 目标文件的文件名

> targets.files[x].render 转换的模板，使用lodash的template语法

可以对lib/fun中的方法进行扩展，以实现更多的功能