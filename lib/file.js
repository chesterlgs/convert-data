let fs = require('fs');
const beautify = require('js-beautify').js_beautify;
function writeFile(path, filename, content) {
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
  }
  fs.writeFileSync(path + filename, content, { encoding: 'utf8' });
}
exports.writeFile = writeFile;

function beautifyJs(content) {
  content = content
    .replace(/(\r\n|\n)\s*/g, '\n')
    .replace(/\(\n/g, '(')
    .replace(/,\n/g, ',')
    .replace(/\/\*\*/g, '\n/**')
    .replace(/\n\/\//g, '\n\n//')
    .replace(/\"/g, "'");

  return beautify(content, {
    indent_with_tabs: false,
    indent_size: 2,
    jslint_happy: true,
    end_with_newline: true,
    space_after_anon_function: true
  });
}
exports.beautifyJs = beautifyJs;
